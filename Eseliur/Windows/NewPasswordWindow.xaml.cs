﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Eseliur.Util;

namespace Eseliur.Windows
{
    /// <summary>
    ///     New password window
    /// </summary>
    public partial class NewPasswordWindow
    {
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register(
            nameof(Password), typeof(Password), typeof(NewPasswordWindow), new PropertyMetadata(default(Password)));

        private ObservableCollection<string> _sections;

        /// <summary>
        ///     Initializes a new instance of <see cref="NewPasswordWindow" />
        /// </summary>
        /// <param name="knownPasswords">list of known passwords so that sections can be governed</param>
        public NewPasswordWindow(IEnumerable<Password> knownPasswords)
        {
            InitializeComponent();
            var passwords = knownPasswords as IList<Password> ?? knownPasswords.ToList();

            _sections = new ObservableCollection<string>(EditSectionsWindow.ExtractSections(passwords));
            SectionCombo.ItemsSource = _sections;
            Password = new Password();
        }

        /// <summary>
        ///     Gets or sets the password in window's editor. Guarantied not to be
        ///     <see langword="null" /> .
        /// </summary>
        public Password Password
        {
            get { return (Password)GetValue(PasswordProperty); }
            set
            {
                if (value == null) return;
                SetValue(PasswordProperty, value);
            }
        }

        private void AddSection(object sender, ExecutedRoutedEventArgs e)
        {
            Dialogs.String(name =>
            {
                if(_sections.Contains(name)) return;
                _sections.Add(name);
                SectionCombo.SelectedValue = name;
            }, null, Properties.Resources.new_name);
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SectionCombo.SelectedItem != null && !string.IsNullOrWhiteSpace(Password.Name);
        }
    }
}
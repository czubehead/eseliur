﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Eseliur.Windows
{
    /// <summary>
    /// A window for editing <see cref="UserField"/>s
    /// </summary>
    public partial class FieldWindow
    {
        public static readonly DependencyProperty FieldProperty = DependencyProperty.Register(
            nameof(Field), typeof(UserField), typeof(FieldWindow), new PropertyMetadata(default(UserField)));

        /// <summary>
        /// Gets or sets the field editing
        /// </summary>
        public UserField Field
        {
            get { return (UserField) GetValue(FieldProperty); }
            set { SetValue(FieldProperty, value); }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="FieldWindow"/>
        /// </summary>
        public FieldWindow(UserField field)
        {
            Field = field;
            InitializeComponent();
        }
    }
}

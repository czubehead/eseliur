﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Eseliur.Util;
using Prop = Eseliur.Properties;

namespace Eseliur.Windows
{
    /// <summary>
    /// Interaction logic for MsgBox.xaml
    /// </summary>
    public partial class MsgBox : Window
    {
        private static readonly string Yes = Prop.Resources.yes;
        private static readonly string No = Prop.Resources.no;
        private static readonly string Ok = Prop.Resources.ok;
        private static readonly string Cancel = Prop.Resources.cancel;

        /// <summary>
        /// Mode in which buttons are rendered
        /// </summary>
        public enum Mode
        {
            /// <summary>
            /// Buttons are rendered as specified in <see cref="MsgBox.Buttons"/>
            /// </summary>
            StandardButtons,
            /// <summary>
            /// Buttons are rendered according to <see cref="CustomButtons"/>
            /// </summary>
            CustomButtons
        }

        /// <summary>
        /// Gets or sets whether custom or standard buttons are rendered
        /// </summary>
        public Mode ButtonsMode { get; set; }

        /// <summary>
        /// <c>List</c> of custom buttons
        /// </summary>
        public IEnumerable<string> CustomButtons { get; set; }

        /// <summary>
        /// Gets or sets the text displayed inside <see cref="MsgBox"/>
        /// </summary>
        public string Text
        {
            get { return MessageTb.Text; }
            set { MessageTb.Text = value; }
        }

        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public string Caption
        {
            get { return Title; }
            set { Title = string.IsNullOrEmpty(value) ? " " : value; }
        }

        /// <summary>
        /// Gets the result of dialog. Set after dialog is closed.
        /// </summary>
        public MessageBoxResult Result { get; private set; } = MessageBoxResult.None;

        /// <summary>
        /// Gets the <see cref="CustomResult"/> in case dialog wasn't carried out properly
        /// </summary>
        public const int InvalidCustomResult = -1;

        /// <summary>
        /// Gets zero-based index of custom button that was pressed
        /// </summary>
        public int CustomResult { get; private set; } = InvalidCustomResult;

        /// <summary>
        /// Gets or sets which buttons are displayed
        /// </summary>
        public MsgBoxButtons Buttons { get; set; }

        /// <summary>
        /// Gets or sets the image displayed alongside text
        /// </summary>
        public MsgBoxImage Image { get; set; }

        /// <summary>
        /// Initializes a new instance of <see cref="MsgBox"/> with standard buttons
        /// </summary>
        /// <param name="buttons"></param>
        /// <param name="image"></param>
        public MsgBox(MsgBoxButtons buttons, MsgBoxImage image = MsgBoxImage.None)
        {
            InitializeComponent();
            Buttons = buttons;
            Image = image;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="MsgBox"/> with custom buttons
        /// </summary>
        /// <param name="customButtons"></param>
        /// <param name="image"></param>
        public MsgBox(IEnumerable<string> customButtons,MsgBoxImage image=MsgBoxImage.None)
        {
            InitializeComponent();

            CustomButtons = customButtons;
            Image = image;
            ButtonsMode = Mode.CustomButtons;
        }

        /// <summary>
        /// Shows the window
        /// </summary>
        public new void Show()
        {
            RenderButtons();
            RenderImages();

            base.Show();
        }

        /// <summary>
        /// Shows the window as a dialog
        /// </summary>
        /// <returns></returns>
        public new bool? ShowDialog()
        {
            RenderButtons();
            RenderImages();

            return base.ShowDialog();
        }

        private void RenderButtons()
        {
            //button texts
            string[] names;

            #region getting buttons' text

            //when standard buttons or no custom buttons
            if (ButtonsMode == Mode.StandardButtons || (!CustomButtons?.Any() ?? true))
            {
                switch (Buttons)
                {
                    default:
                        names = new[] { Ok };
                        break;

                    case MsgBoxButtons.OkCancel:
                        names = new[] { Ok, Cancel };
                        break;

                    case MsgBoxButtons.YesNo:
                        names = new[] { Yes, No };
                        break;

                    case MsgBoxButtons.YesNoCancel:
                        names = new[] { Yes, No, Cancel };
                        break;
                }
            }
            else
            {
                names = CustomButtons.ToArray();
            }

            #endregion

            BtnGrid.ColumnDefinitions.Clear();
            BtnGrid.Children.Clear();

            for (int i = 0; i < names.Length; i++)
            {
                //add a column for every button
                BtnGrid.ColumnDefinitions.Add(
                    new ColumnDefinition
                    {
                        Width = new GridLength(1, GridUnitType.Star)
                    });

                Button btn = new Button
                {
                    Content = names[i],
                    //Tag is set to either MessageBoxResult or number of image
                    Tag = ButtonsMode == Mode.StandardButtons
                        ? (object)ResultFromName(names[i]) : i,
                    TabIndex = i
                };
                if (i == 0)
                {
                    btn.IsDefault = true;//first button in default
                    btn.Focus();
                }
                if (names[i] == Prop.Resources.cancel && !btn.IsDefault)
                    btn.IsCancel = true;//cancel is cancel unless it had already been set as default

                btn.Click += Button_Click;

                Grid.SetColumn(btn, i);
                BtnGrid.Children.Add(btn);
            }
        }

        private void RenderImages()
        {
            object imageContent;
            switch (Image)
            {
                case MsgBoxImage.Error:
                    imageContent = FindResource(Prop.Keys.error_icon);
                    break;

                case MsgBoxImage.Info:
                    imageContent = FindResource(Prop.Keys.info_icon);
                    break;

                case MsgBoxImage.Question:
                    imageContent = FindResource(Prop.Keys.question_icon);
                    break;

                case MsgBoxImage.Warning:
                    imageContent = FindResource(Prop.Keys.warning_icon);
                    break;

                // ReSharper disable once RedundantCaseLabel
                case MsgBoxImage.None:
                default:
                    imageContent = null;
                    break;
            }

            IconBox.Child = ((UIElement)imageContent).Clone();
        }

        /// <summary>
        /// Handles click by one of 1-3 buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="routedEventArgs"></param>
        private void Button_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            object tag = (sender as Button)?.Tag;
            if (tag != null)
                if (tag is MessageBoxResult)
                {
                    //standard buttons have result in their Tag
                    Result = (MessageBoxResult)tag;
                }
                else if (tag is int)
                {
                    //custom have their index
                    CustomResult = (int)tag;
                }

            DialogResult = true;
            Close();
        }

        private MessageBoxResult ResultFromName(string name)
        {
            if (name == Yes) return MessageBoxResult.Yes;
            if (name == No) return MessageBoxResult.No;
            if (name == Ok) return MessageBoxResult.OK;
            if (name == Cancel) return MessageBoxResult.Cancel;
            return MessageBoxResult.None;
        }

        /// <summary>
        /// Focus first button (which is default)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MsgBox_OnActivated(object sender, EventArgs e)
        {
            if (BtnGrid.Children.Count == 0) return;

            var first = BtnGrid.Children[0] as Button;
            first?.Focus();
        }
    }

    /// <summary>
    /// Buttons for <see cref="MsgBox"/>
    /// </summary>
    public enum MsgBoxButtons
    {
        /// <summary>
        /// Single ok button
        /// </summary>
        Ok,
        /// <summary>
        /// Ok and cancel buttons
        /// </summary>
        OkCancel,
        /// <summary>
        /// Yes and no buttons
        /// </summary>
        YesNo,
        /// <summary>
        /// Yes, no and cancel buttons
        /// </summary>
        YesNoCancel,
    }

    /// <summary>
    /// Images for <see cref="MsgBox"/>
    /// </summary>
    public enum MsgBoxImage
    {
        /// <summary>
        /// Error image
        /// </summary>
        Error,
        /// <summary>
        /// Info image
        /// </summary>
        Info,
        /// <summary>
        /// Warning image
        /// </summary>
        Warning,
        /// <summary>
        /// Question image
        /// </summary>
        Question,
        /// <summary>
        /// No image
        /// </summary>
        None
    }
}

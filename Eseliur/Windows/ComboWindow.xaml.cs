﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Eseliur.Windows
{
    /// <summary>
    /// A windows allowing a simple string input
    /// </summary>
    public partial class ComboWindow
    {
        /// <summary>
        /// Gets or sets the text in input
        /// </summary>
        public object Selected => Combo.SelectedItem;

        /// <summary>
        /// Initializes a new instance of <see cref="StringInputWindow"/>
        /// </summary>
        public ComboWindow(IEnumerable<object> source)
        {
            InitializeComponent();

            Combo.ItemsSource = source;

            Activated += (sender, args) => Combo.Focus();
        }
    }
}

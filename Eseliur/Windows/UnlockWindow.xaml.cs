﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Eseliur.Windows
{
    /// <summary>
    /// A windows that has single password input
    /// </summary>
    public partial class UnlockWindow
    {
        /// <summary>
        /// Gets or sets the password in <see cref="PasswordBox"/>
        /// </summary>
        public string Password
        {
            get { return PwdBox.Password; }
            set { PwdBox.Password = value; }
        }

        /// <summary>
        /// Gets or sets if password was incorrect and user should be notified
        /// </summary>
        public bool WasIncorrect
        {
            get { return PwdBox.ShowIncorrect; }
            set { PwdBox.ShowIncorrect = value; }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="UnlockWindow"/>
        /// </summary>
        public UnlockWindow()
        {
            InitializeComponent();

            PwdBox.PasswordChanged += (sender, s) => Password = s;
            PwdBox.Password = Password;

            Activated += (sender, args) => PwdBox.Focus();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = (sender as Button)?.IsDefault ?? false;

            Close();
        }
    }
}

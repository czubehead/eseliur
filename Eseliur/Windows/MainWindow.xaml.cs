﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using CsEncryption;
using Eseliur.Properties;
using Eseliur.Util;

namespace Eseliur.Windows
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private PasswordCollection _passwords;

        /// <summary>
        ///     Initializes a new instance of <see cref="MainWindow" />
        /// </summary>
        /// <param name="saveLocation">where to load from</param>
        public MainWindow(Uri saveLocation = null)
        {
            InitializeComponent();
            InitializeCommands();

            Load(saveLocation);            
        }

        /// <summary>
        ///     Gets the location file is saved at
        /// </summary>
        public Uri SaveLocation { get; private set; }

        /// <summary>
        ///     Gets or sets the file is encrypted with
        /// </summary>
        public string MasterPassword { get; set; }

        /// <summary>
        ///     Gets if a file is loaded.
        /// </summary>
        /// <remarks>
        ///     <see cref="Eseliur.Windows.MainWindow.SaveLocation" /> may be null!
        /// </remarks>
        public bool FileLoaded { get; private set; }

        /// <summary>
        ///     Gets merged list of passwords.
        /// </summary>
        /// <remarks>
        ///     Created by <see cref="MergeSections" />
        /// </remarks>
        public PasswordCollection Passwords
        {
            get { return _passwords; }
            private set
            {
                _passwords = value;
                PasswordList.ItemsSource = _passwords;

                #region PasswordList grouping

                var view = (CollectionView)CollectionViewSource.GetDefaultView(Passwords);
                var groupper = new PropertyGroupDescription(nameof(Password.Section));
                view.GroupDescriptions?.Add(groupper);

                #endregion
            }
        }

        /// <summary>
        ///     Loads data from specified <see cref="Uri" />
        /// </summary>
        /// <param name="loadFrom">uri to load from</param>
        public void Load(Uri loadFrom)
        {

            FileLoaded = false;
            MasterPassword = null;
            Passwords = new PasswordCollection();
            Workspace.IsEnabled = false;

            if (loadFrom == null) return;

            PasswordLoader loader;
            var leave = false;
            //how many bad attempts has user had with password
            var attempts = 1;
            while (!leave)
                //ask for password until it is correct
                Dialogs.Unlock(password =>
                    {
                        loader = new PasswordLoader(password);

                        try
                        {
                            Passwords = new PasswordCollection(loader.Load(loadFrom));
                            SaveLocation = loadFrom;
                            leave = true;
                            FileLoaded = true;
                            Workspace.IsEnabled = true;
                            MasterPassword = password;
                        }
                        catch (IOException)
                        {
                            //file inaccessible
                            Dialogs.Message("File is inaccessible", MsgBoxImage.Error);
                            FileLoaded = false;
                            MasterPassword = null;
                        }
                        catch (IncorrectPasswordException)
                        {
                            //bad password but file is accessible
                            attempts++;
                        }
                        finally
                        {
                            loader.Dispose();
                        }
                    },
                    () => leave = true, attempts > 1);
        }

        /// <summary>
        ///     Saves data to specified <see cref="Uri" />
        /// </summary>
        /// <param name="saveTo">where to save data</param>
        /// <returns>
        ///     If saving was successful
        /// </returns>
        private bool Save(Uri saveTo)
        {
            if (saveTo == null) throw new ArgumentNullException(nameof(saveTo));

            var success = true;
            PasswordLoader loader = null;

            if (string.IsNullOrEmpty(MasterPassword))
            {
                //ask for password
                Dialogs.Unlock(
                    password =>
                    {
                        loader = new PasswordLoader(password);
                    },
                    () => success = false);
            }
            else
                loader = new PasswordLoader(MasterPassword);

            if (!success || loader == null) return false;

            //repeat for retry
            using (loader)
            {
                bool repeat = false;
                do
                {
                    try
                    {
                        loader.Save(saveTo, Passwords);
                    }
                    catch (IOException)
                    {
                        //unable to write to the file
                        //show a dialog asking whether retry or cancel
                        Dialogs.MessageCustom(Properties.Resources.unable_to_write_changes, new[]
                            {
                            Properties.Resources.retry,
                            Properties.Resources.cancel
                        },
                        btnIndex => repeat = btnIndex == 0, 
                        () => repeat = false, null, MsgBoxImage.Error);
                    }
                } while (repeat);
            }

            return true;
        }

        /// <summary>
        ///     Allows a command execution no matter consequences
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Always_CanExecute(object sender, CanExecuteRoutedEventArgs e)
            => e.CanExecute = true;

        /// <summary>
        ///     Allows command execution when a file is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileLoaded_CanExecute(object sender, CanExecuteRoutedEventArgs e)
            => e.CanExecute = FileLoaded;

        /// <summary>
        ///     Prompts user to save changes. Returns if it is safe to continue
        /// </summary>
        /// <returns>
        ///     whether it is safe to proceed
        /// </returns>
        private bool SaveChangesPrompt()
        {
            if (SaveLocation == null)
                throw new InvalidOperationException($"{nameof(SaveLocation)} {Internal._cannot_be_null}");

            var result = MessageBoxResult.None;
            Dialogs.Message("Would you like to save changes made to the file?", q => result = q,
                null, MsgBoxImage.Question, MsgBoxButtons.YesNoCancel);
            if (result == MessageBoxResult.Yes)
                Save(SaveLocation);
            else if (result == MessageBoxResult.Cancel)
                return false;

            return true;
        }

        /// <summary>
        ///     Carry out commands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            //Execute the CommandDelegate associated with command
            if (e.Command == ApplicationCommands.Delete)
            {
                Delete(e.Parameter);
                return;
            }
            Commands[e.Command]();
        }
    }
}
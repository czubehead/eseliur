﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Eseliur.Windows
{
    /// <summary>
    /// A windows allowing a simple string input
    /// </summary>
    public partial class StringInputWindow : Window
    {
        /// <summary>
        /// Gets or sets the text in input
        /// </summary>
        public string Text
        {
            get { return TbxBox.Text; }
            set { TbxBox.Text = value; }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="StringInputWindow"/>
        /// </summary>
        public StringInputWindow()
        {
            InitializeComponent();

            Activated += (sender, args) => TbxBox.Focus();
        }
    }
}

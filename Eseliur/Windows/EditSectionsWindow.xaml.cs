﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Eseliur.Util;

namespace Eseliur.Windows
{
    /// <summary>
    /// Lets user manage section among given list of passwords
    /// </summary>
    public partial class EditSectionsWindow
    {
        private readonly IEnumerable<Password> _passwords;

        private List<SectionModel> _sections;

        /// <summary>
        /// Initializes a new instance of <see cref="EditSectionsWindow"/>
        /// </summary>
        /// <param name="passwords">passwords to manage</param>
        public EditSectionsWindow(IEnumerable<Password> passwords)
        {
            InitializeComponent();

            Buttons.Dialog = this;

            var enumerablePasswords = passwords as Password[] ?? passwords.ToArray();
            _passwords = enumerablePasswords;

            _sections = ExtractSections(enumerablePasswords).Select(q => (SectionModel)q).ToList();
        }

        /// <summary>
        /// Extracts all sections from given list
        /// </summary>
        /// <param name="passwords">to extract from</param>
        /// <returns></returns>
        public static IEnumerable<string> ExtractSections(IEnumerable<Password> passwords)
        {
            HashSet<string> known = new HashSet<string>();
            foreach (var password in passwords)
            {
                known.Add(password.Section);
            }
            return known;
        }

        /// <summary>
        /// Finds a section by <paramref name="name"/> and if found, carries out <paramref name="action"/>
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        private void SectionDo(string name, Action<SectionModel> action)
        {
            var match = _sections.Where(q => q.Name == name).ToArray();
            if (match.Any())
                action?.Invoke(match.ElementAt(0));
        }

        private void RenameButton_OnClick(object sender, RoutedEventArgs e)
        {
            Button sent = sender as Button;
            if (sent == null) return;

            Dialogs.String(str =>
            SectionDo(sent.Tag.ToString(), q => q.NewName = str));
        }

        private void MergeButton_OnClick(object sender, RoutedEventArgs e)
        {
            Button sent = sender as Button;
            if (sent == null) return;

            string from = sent.Tag.ToString();
            Dialogs.Combo(_sections.SkipWhile(q => q.Name == from), selected =>
                     SectionDo(from, q =>
                     {
                         q.MoveTo = ((SectionModel)selected).Name;
                     }), null, Properties.Resources.merge_with);
            //set source's MoveTo to selected Name
        }

        private void Add_section(object sender, RoutedEventArgs e) =>
            Dialogs.String(q => _sections.Add(q),null,Properties.Resources.new_name);

        private void ResultButton_OnClick(object sender, RoutedEventArgs e)
        {

        }
    }

    /// <summary>
    /// Model used to describe a section for binding
    /// </summary>
    class SectionModel
    {
        /// <summary>
        /// Gets or sets the old name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the name it should be renamed to
        /// </summary>
        public string NewName { get; set; }

        /// <summary>
        /// Gets or sets the name of the section current should be merged with
        /// </summary>
        public string MoveTo { get; set; }

        /// <summary>
        /// Converts a string to <see cref="SectionModel"/>
        /// </summary>
        /// <param name="s"></param>
        public static implicit operator SectionModel(string s) => new SectionModel
        {
            Name = s,
            NewName = s,
            MoveTo = null
        };
    }
}

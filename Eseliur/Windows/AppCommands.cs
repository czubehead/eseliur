﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Eseliur.Windows
{
    /// <summary>
    /// Collection of custom commands
    /// </summary>
    public static class AppCommands
    {
        /// <summary>
        /// Command for new file
        /// </summary>
        public static readonly RoutedUICommand NewFile=new RoutedUICommand(
            "NewFile",
            "NewFile",
            typeof(AppCommands),
            new InputGestureCollection
            {
                //CTRL+SHIFT+N
                new KeyGesture(Key.N,ModifierKeys.Control|ModifierKeys.Shift)
            });

        /// <summary>
        /// Command for new user field
        /// </summary>
        public static readonly RoutedUICommand NewField=new RoutedUICommand(
            "NewField",
            "NewField",
            typeof(AppCommands),
            new InputGestureCollection()
            {
                new KeyGesture(Key.A,ModifierKeys.Control)
            });

        /// <summary>
        /// Command for password sections editor
        /// </summary>
        public static readonly RoutedUICommand EditSections = new RoutedUICommand(
            "EditSections",
            "EditSections",
            typeof(AppCommands));

        /// <summary>
        /// Command for password sections editor
        /// </summary>
        public static readonly RoutedUICommand Ok = new RoutedUICommand(
            "Ok",
            "Ok",
            typeof(AppCommands));

        /// <summary>
        /// Command for password sections editor
        /// </summary>
        public static readonly RoutedUICommand Cancel = new RoutedUICommand(
            "Cancel",
            "Cancel",
            typeof(AppCommands));

        /// <summary>
        /// Command for password sections editor
        /// </summary>
        public static readonly RoutedUICommand ChangePassword = new RoutedUICommand(
            "ChangePassword",
            "ChangePassword",
            typeof(AppCommands));
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Eseliur.Properties;
using Eseliur.Util;

namespace Eseliur.Windows
{
    /// <summary>
    /// This file handles commands
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Command bindings
        /// </summary>
        private Dictionary<ICommand, Action> Commands { get; set; }

        /// <summary>
        /// Bind commands to their methods (fill <see cref="Commands"/>)
        /// </summary>
        private void InitializeCommands()
        {
            Commands = new Dictionary<ICommand, Action>
            {
                {ApplicationCommands.Save,File_Save },
                {ApplicationCommands.SaveAs,File_SaveAs },
                {ApplicationCommands.Open,File_Open },
                {AppCommands.NewFile,File_New },
                {ApplicationCommands.New, AddPassword},
                {AppCommands.ChangePassword,ChangePassword }
            };
        }

        /// <summary>
        /// File > change password
        /// </summary>
        private void ChangePassword()
        {
            bool fine = MasterPassword == null;
            if (MasterPassword != null)
                Dialogs.Unlock(old =>
                {
                    fine = old == MasterPassword;
                }, null, false, Properties.Resources.enter_old_password);
            if (!fine)
            {
                Dialogs.Message(Properties.Resources.password_was_incorrect, MsgBoxImage.Error);
                return;
            }

            Dialogs.Unlock(newPwd => MasterPassword = newPwd,
                null, false, Properties.Resources.new_password);
        }

        /// <summary>
        /// PasswordList > delete. This has a special case, not included in <see cref="InitializeCommands"/>
        /// </summary>
        private void Delete(object param)
        {
            Password item = param as Password;
            if (item == null) return;

            Dialogs.Confirm(() => Passwords.Remove(item));
        }

        /// <summary>
        /// File_Open file
        /// </summary>
        private void File_Open()
        {
            Dialogs.OpenFile(Internal.file_filter, file =>
            {
                Load(new Uri(file));
            });
        }

        /// <summary>
        /// Add new password
        /// </summary>
        private void AddPassword()
            => Dialogs.NewPassword(Passwords, password => Passwords.Add(password));

        /// <summary>
        /// File > new
        /// </summary>
        private void File_New()
        {
            if (SaveLocation != null && !SaveChangesPrompt())//save work
            {
                Dialogs.Message(Properties.Resources.changes_were_not_saved, MsgBoxImage.Info);
                return;
            }

            MasterPassword = null;
            SaveLocation = null;

            Passwords = new PasswordCollection();
            Workspace.IsEnabled = true;
            FileLoaded = true;
        }

        /// <summary>
        /// File > save
        /// </summary>
        private void File_Save()
        {
            if (SaveLocation == null)
                File_SaveAs();
            else
                Save(SaveLocation);
        }

        /// <summary>
        /// File > File_SaveAs
        /// </summary>
        private void File_SaveAs()
        {
            Dialogs.SaveFile(Internal.file_filter, file => Save(new Uri(file)));
        }
    }
}

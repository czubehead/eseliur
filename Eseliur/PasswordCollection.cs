﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eseliur
{
    /// <summary>
    /// An observable collection of passwords. CollectionChanged is fired even if just a property of one item changes
    /// </summary>
    public class PasswordCollection : ObservableCollection<Password>
    {
        /// <summary>
        /// Adds a new <paramref name="item"/> to collection
        /// </summary>
        /// <param name="item"></param>
        public new void Add(Password item)
        {
            item.PropertyChanged -= ItemOnPropertyChanged;
            item.PropertyChanged += ItemOnPropertyChanged;
            base.Add(item);
        }

        /// <summary>
        /// Removes <paramref name="item"/> from collection
        /// </summary>
        /// <param name="item"></param>
        public new void Remove(Password item)
        {
            item.PropertyChanged -= ItemOnPropertyChanged;
            base.Remove(item);
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PasswordCollection"/> from another collection
        /// </summary>
        /// <param name="source"></param>
        public PasswordCollection(IEnumerable<Password> source = null)
        {
            if (source == null) return;

            foreach (var password in source)
                Add(password);
        }

        private void ItemOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, new[] { sender }, new[] { sender });
            OnCollectionChanged(args);
        }
    }
}

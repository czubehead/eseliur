﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Eseliur.Windows;

namespace Eseliur.UserControls
{
    /// <summary>
    ///     Ok and cancel buttons
    /// </summary>
    public partial class OkCancelButtons
    {
        /// <summary>
        ///     Initializes a new instance of <see cref="OkCancelButtons" />
        /// </summary>
        public OkCancelButtons()
        {
            InitializeComponent();
            
            OkBinding.CanExecute += (sender, args) =>
            {
                if (OkCanExecute != null)
                    OkCanExecute.Invoke(sender, args);
                else
                    args.CanExecute = true;
            };

            Loaded += (sender, args) => Dialog=Window.GetWindow(this);
        }

        /// <summary>
        ///     Gets or sets the dialog <see langword="this" /> is placed in. This is set automatically
        /// </summary>
        public Window Dialog { get; set; }

        /// <summary>
        ///     En event controlling when is it possible to press ok
        /// </summary>
        public event CanExecuteRoutedEventHandler OkCanExecute;

        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (Dialog == null)
                return;

            Dialog.DialogResult = e.Command == AppCommands.Ok;
            Dialog.Close();
        }
    }
}
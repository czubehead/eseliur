﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Eseliur.UserControls
{
    /// <summary>
    /// An extended PasswordBox which lets users display password in plaintext
    /// </summary>
    public partial class PasswordBox
    {
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register(
            nameof(Password), typeof(string), typeof(PasswordBox), new PropertyMetadata(default(string)));

        public static readonly DependencyProperty IsCopyEnabledProperty = DependencyProperty.Register(
            nameof(IsCopyEnabled), typeof(bool), typeof(PasswordBox), new PropertyMetadata(default(bool)));

        /// <summary>
        /// <see cref="Password"/> has changed
        /// </summary>
        public event EventHandler<string> PasswordChanged;

        /// <summary>
        /// Gets or sets whether copy button for password is visible
        /// </summary>
        public bool IsCopyEnabled
        {
            get { return (bool)GetValue(IsCopyEnabledProperty); }
            set { SetValue(IsCopyEnabledProperty, value); }
        }

        /// <summary>
        /// Whether to react on password box changes
        /// </summary>
        private bool _react = true;

        private bool _showIncorrect;

        /// <summary>
        /// Password to display. Is hidden upon setting
        /// </summary>
        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                SetValue(PasswordProperty, value);
                HidePassword();
            }
        }

        /// <summary>
        /// Gets or sets if input borders should be red
        /// </summary>
        public bool ShowIncorrect
        {
            get { return _showIncorrect; }
            set
            {
                _showIncorrect = value;
                if (value)
                {
                    PwdBox.BorderBrush = Brushes.Red;
                    TxtBox.BorderBrush = Brushes.Red;

                    PwdBox.Background = new SolidColorBrush(Color.FromArgb(24, byte.MaxValue, 0, 0));
                }
                else
                {
                    PwdBox.ClearValue(BorderBrushProperty);
                    TxtBox.ClearValue(BorderBrushProperty);

                    PwdBox.ClearValue(BackgroundProperty);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PasswordBox"/>
        /// </summary>
        public PasswordBox()
        {
            InitializeComponent();

            ViewButton.PreviewMouseDown += (sender, args) => ShowPassword();
            ViewButton.PreviewMouseUp += (sender, args) => HidePassword();
            ViewButton.MouseLeave += (sender, args) => HidePassword();

            #region enable PwdBox copy

            IsCopyEnabled = true;

            CommandBinding cb = new CommandBinding(ApplicationCommands.Copy, CopyToClipboard_Click);
            PwdBox.CommandBindings.Add(cb);

            #endregion
        }

        /// <summary>
        /// Attempts to set focus
        /// </summary>
        /// <returns></returns>
        public new bool Focus() => PwdBox.Focus();

        /// <summary>
        /// Display in plaintext
        /// </summary>
        public void ShowPassword()
        {
            PwdBox.Visibility = Visibility.Collapsed;
            TxtBox.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Display only the dots
        /// </summary>
        public void HidePassword()
        {
            PwdBox.Visibility = Visibility.Visible;
            TxtBox.Visibility = Visibility.Collapsed;
        }

        #region manual binding

        private void TextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_react) return;

            _react = false;
            PwdBox.Password = TxtBox.Text;
            _react = true;

            PasswordChanged?.Invoke(this, Password);
        }

        private void PwdBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (!_react) return;

            _react = false;
            Password = PwdBox.Password;
            _react = true;

            PasswordChanged?.Invoke(this, Password);
        }

        #endregion

        /// <summary>
        /// Handle copying to clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyToClipboard_Click(object sender, RoutedEventArgs e)
        {
            if (!IsCopyEnabled) return;
            Clipboard.SetText(Password);
        }
    }
}

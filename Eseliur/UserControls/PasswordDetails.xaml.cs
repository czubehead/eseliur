﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Eseliur.Windows;

namespace Eseliur.UserControls
{
    /// <summary>
    /// UI <see cref="Password"/> editor
    /// </summary>
    public partial class PasswordDetails : UserControl
    {
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register(
            nameof(Password), typeof(Password), typeof(PasswordDetails), new PropertyMetadata(default(Password)));

        /// <summary>
        /// Gets or sets the password that is edited
        /// </summary>
        public Password Password
        {
            get { return (Password) GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="PasswordDetails"/> with a new instance of <see cref="Password"/>
        /// </summary>
        public PasswordDetails()
        {
            InitializeComponent();

            Password=new Password();
        }

        /// <summary>
        /// Command can be executed no matter what is going on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Always_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute=true;

        /// <summary>
        /// Handles new field button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if(e.Command!=AppCommands.NewField) return;

            FieldWindow win = new FieldWindow(new UserField());
            if (win.ShowDialog() == true && !string.IsNullOrWhiteSpace(win.Field.Label))
            {
                Password.Fields.Add(win.Field);
            }
        }
    }
}

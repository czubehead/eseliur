﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CsEncryption;
using Eseliur.Properties;

namespace Eseliur
{
    /// <summary>
    /// Provides a way to save/load password data
    /// </summary>
    class PasswordLoader : IDisposable
    {
        private readonly Password256Aes _aes;

        /// <summary>
        /// Initializes new instance of <see cref="PasswordLoader"/> with given password
        /// </summary>
        /// <param name="password">password used for encrypting/decrypting objects</param>
        public PasswordLoader(string password)
        {
            _aes = new Password256Aes(password);
        }

        /// <summary>
        /// Load PasswordSections to list from specified <see cref="Uri"/>
        /// </summary>
        /// <param name="location">location to load from</param>
        /// <returns></returns>
        public List<Password> Load(Uri location)
        {
            byte[] cipher = null;
            if (location.Scheme == Uri.UriSchemeFile)
                cipher = File.ReadAllBytes(location.LocalPath);
            else if (location.Scheme == Uri.UriSchemeHttp || location.Scheme == Uri.UriSchemeHttps)
                using (WebClient client = new WebClient())
                    cipher = client.DownloadData(location);

            if (cipher == null || cipher.Length == 0)
                throw new ArgumentException(Resources.PasswordLoader_Load_Invalid_location, nameof(location));

            return _aes.Decrypt<List<Password>>(cipher);
        }

        /// <summary>
        /// Save <paramref name="data"/> to location, encrypted
        /// </summary>
        /// <param name="location">where to save</param>
        /// <param name="data">what to save encrypted</param>
        public void Save(Uri location, IEnumerable<Password> data)
        {
            if (location.Scheme != Uri.UriSchemeFile)
                throw new ArgumentException(Resources.PasswordLoader_Load_Invalid_location, nameof(location));

            File.WriteAllBytes(location.LocalPath, _aes.Encrypt(data.ToList()));
        }

        ///<inheritdoc/>
        public void Dispose()
        {
            _aes.Dispose();
        }
    }
}

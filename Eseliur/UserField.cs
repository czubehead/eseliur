﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eseliur
{
    /// <summary>
    /// Represents an additional field user may add for a password
    /// </summary>
    [Serializable]
    public class UserField
    {
        /// <summary>
        /// Gets or sets the label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets if is multiline
        /// </summary>
        public bool IsMultiline { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using Eseliur.Properties;
using Eseliur.Windows;
using Microsoft.Win32;

namespace Eseliur.Util
{
    /// <summary>
    ///     Provides a simplified way to call dialogs
    /// </summary>
    internal static class Dialogs
    {
        /// <summary>
        ///     Default filter for saving/opening dialogs (all files)
        /// </summary>
        public static readonly string DefaultFileFilter = $"{Resources.all_files} (*.*)|*.*";

        /// <summary>
        ///     Shows a windows prompting for a password
        /// </summary>
        /// <param name="success">what to do when clicked <c>ok</c></param>
        /// <param name="canceled">what to do when <c>canceled</c></param>
        /// <param name="wasIncorrect">whether password was previously incorrect</param>
        /// <param name="caption">title</param>
        public static void Unlock(Action<string> success, Action canceled = null, bool wasIncorrect = false, string caption = null)
        {
            var win = new UnlockWindow { WasIncorrect = wasIncorrect };
            if (!string.IsNullOrEmpty(caption))
                win.Title = caption;

            if (win.ShowDialog() == true && win.Password != null)
                success?.Invoke(win.Password);
            else
                canceled?.Invoke();
        }

        /// <summary>
        ///     Shows <see cref="Dialogs.SaveFile" /> dialog
        /// </summary>
        /// <param name="fileFilter">
        ///     file filter. Pass <see langword="null" /> to set to all files
        /// </param>
        /// <param name="success">what to do when succeeded</param>
        /// <param name="canceled">what to do when canceled</param>
        public static void SaveFile(string fileFilter, Action<string> success, Action canceled = null)
        {
            var filter = fileFilter ?? DefaultFileFilter;
            var dlg = new SaveFileDialog { Filter = filter };

            if (dlg.ShowDialog() == true)
                success?.Invoke(dlg.FileName);
            else
                canceled?.Invoke();
        }

        /// <summary>
        ///     Shows <see cref="OpenFileDialog" />
        /// </summary>
        /// <param name="fileFilter">
        ///     file filter. Pass <see langword="null" /> to set to all files
        /// </param>
        /// <param name="success">what to do when succeeded</param>
        /// <param name="canceled">what to do when <c>canceled</c></param>
        public static void OpenFile(string fileFilter, Action<string> success, Action canceled = null)
        {
            var filter = fileFilter ?? DefaultFileFilter;
            var dlg = new OpenFileDialog { Filter = filter };

            if (dlg.ShowDialog() == true)
                success?.Invoke(dlg.FileName);
            else
                canceled?.Invoke();
        }

        /// <summary>
        ///     Shows <see cref="MsgBox" />
        /// </summary>
        /// <param name="text">message</param>
        /// <param name="success">what to do when not closed</param>
        /// <param name="caption">window title</param>
        /// <param name="image">icon</param>
        /// <param name="buttons">which buttons to disply</param>
        public static void Message(string text, Action<MessageBoxResult> success, string caption = null,
            MsgBoxImage image = MsgBoxImage.None,
            MsgBoxButtons buttons = MsgBoxButtons.Ok)
        {
            var box = new MsgBox(buttons, image)
            {
                Text = text,
                Caption = caption
            };

            success?.Invoke(box.ShowDialog() == true ? box.Result : MessageBoxResult.None);
        }

        /// <summary>
        ///     Shows a message without showing result
        /// </summary>
        /// <param name="text">message</param>
        /// <param name="image">type</param>
        public static void Message(string text, MsgBoxImage image = MsgBoxImage.None)
            => Message(text, q => { }, null, image);

        /// <summary>
        ///     Shows <see cref="MsgBox" /> with custom buttons. Should anything go
        ///     wrong, <see cref="Eseliur.Windows.MsgBox.InvalidCustomResult" /> is
        ///     passed to <paramref name="success" /> , index of pressed button
        ///     otherwise.
        /// </summary>
        /// <param name="text">text to display</param>
        /// <param name="buttons">which buttons to display</param>
        /// <param name="success">what to do with the result</param>
        /// <param name="canceled">what to do in case of cancellation</param>
        /// <param name="caption">title</param>
        /// <param name="image">image</param>
        public static void MessageCustom(string text, IEnumerable<string> buttons, Action<int> success,
            Action canceled = null,
            string caption = null, MsgBoxImage image = MsgBoxImage.None)
        {
            var box = new MsgBox(buttons, image)
            {
                Text = text,
                Caption = caption
            };
            var result = box.ShowDialog() == true ? box.CustomResult : MsgBox.InvalidCustomResult;
            if (result != MsgBox.InvalidCustomResult)
                success?.Invoke(result);
            else
                canceled?.Invoke();
        }

        /// <summary>
        /// Displays a <see cref="MsgBox"/> asking a yes/no question, "Are you sure?" by default
        /// </summary>
        /// <param name="yes">when yes</param>
        /// <param name="no">when no or cancel</param>
        /// <param name="text">question. pass null to leave "Are you sure?"</param>
        public static void Confirm(Action yes, Action no = null, string text = null)
        {
            string content = string.IsNullOrEmpty(text) ? Resources.are_you_sure : text;
            Message(content, result =>
            {
                if (result == MessageBoxResult.Yes)
                    yes?.Invoke();
                else
                    no?.Invoke();
            }, null, MsgBoxImage.Question, MsgBoxButtons.YesNo);
        }

        /// <summary>
        /// Shows <see cref="NewPasswordWindow"/> and works with the result
        /// </summary>
        /// <param name="knownPasswords">list of known passwords</param>
        /// <param name="success">what to do with success</param>
        /// <param name="canceled">what to do when canceled</param>
        /// <param name="caption">title</param>
        public static void NewPassword(IEnumerable<Password> knownPasswords, Action<Password> success, Action canceled = null, string caption = null)
        {
            NewPasswordWindow win = new NewPasswordWindow(knownPasswords);
            if (!string.IsNullOrEmpty(caption))
                win.Title = caption;

            if (win.ShowDialog() == true)
                success?.Invoke(win.Password);
            else
                canceled?.Invoke();
        }

        /// <summary>
        /// Shows <see cref="StringInputWindow"/> prompting for string
        /// </summary>
        /// <param name="success">what to do with the string</param>
        /// <param name="canceled">what to do when canceled</param>
        /// <param name="caption">title</param>
        public static void String(Action<string> success, Action canceled = null, string caption = null)
        {
            StringInputWindow win = new StringInputWindow();
            if (!string.IsNullOrEmpty(caption))
                win.Title = caption;

            if (win.ShowDialog() == true && !string.IsNullOrWhiteSpace(win.Text))
                success?.Invoke(win.Text);
            else
                canceled?.Invoke();
        }

        /// <summary>
        /// Shows <see cref="ComboWindow"/> and lets user select an option
        /// </summary>
        /// <param name="source">items to select from</param>
        /// <param name="success">success</param>
        /// <param name="canceled">canceled</param>
        /// <param name="caption">title</param>
        public static void Combo(IEnumerable<object> source, Action<object> success, Action canceled = null,
            string caption = null)
        {
            ComboWindow win = new ComboWindow(source);
            if (!string.IsNullOrWhiteSpace(caption))
                win.Title = caption;

            if (win.ShowDialog() == true && win.Selected != null)
                success?.Invoke(win.Selected);
            else
                canceled?.Invoke();
        }
    }
}
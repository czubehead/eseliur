﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Eseliur.Util
{
    /// <summary>
    /// Allows <see langword="bool"/> to Visibility conversion in either direction
    /// </summary>
    public class BoolVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Converts <see langword="true"/> to Visibility.Visible, false to Visibility.Collapsed and vice versa
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool && targetType == typeof(Visibility))
                return FromBool((bool)value);

            if (value is Visibility && targetType == typeof(Visibility))
                return FromVisibility((Visibility)value);

            throw new NotSupportedException();
        }

        /// <summary>
        /// Converts <see langword="true"/> to Visibility.Visible, false to Visibility.Collapsed and vice versa
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => Convert(value, targetType, parameter, culture);

        private static Visibility FromBool(bool boolean) =>
            boolean ? Visibility.Visible : Visibility.Collapsed;

        private static bool FromVisibility(Visibility visibility)
            => visibility == Visibility.Visible;
    }
}

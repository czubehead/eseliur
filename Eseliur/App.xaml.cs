﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Eseliur.Util;
using Eseliur.Windows;

namespace Eseliur
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Uri openUri = null;
            if (AppDomain.CurrentDomain?.SetupInformation.ActivationArguments?.ActivationData != null)
                foreach (var arg in AppDomain.CurrentDomain.SetupInformation.ActivationArguments?.ActivationData)
                    try
                    {
                        openUri = new Uri(Uri.UnescapeDataString(arg));
                    }
                    catch
                    {/*ignored*/ }
            
            MainWindow mw = new MainWindow(openUri);
            MainWindow = mw;
            mw.Show();
        }
    }
}

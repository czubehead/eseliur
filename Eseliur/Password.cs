﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Eseliur.Annotations;

namespace Eseliur
{
    /// <summary>
    /// Represents a user password
    /// </summary>
    [Serializable]
    public class Password : INotifyPropertyChanged
    {
        private string _name;
        private string _passcode;
        private string _section;
        private ObservableCollection<UserField> _fields = new ObservableCollection<UserField>();

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;

                OnPropertyChanged(nameof(Name));
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the actual password
        /// </summary>
        public string Passcode
        {
            get { return _passcode; }
            set
            {
                if (value == _passcode) return;

                OnPropertyChanged(nameof(Passcode));
                _passcode = value;
            }
        }

        /// <summary>
        /// Gets or sets the section password belongs to
        /// </summary>
        public string Section
        {
            get { return _section; }
            set
            {
                if (value == _section) return;

                OnPropertyChanged(nameof(Section));
                _section = value;
            }
        }

        /// <summary>
        /// Gets or sets user-defined fields
        /// </summary>
        public ObservableCollection<UserField> Fields
        {
            get { return _fields; }
            set
            {
                if (value == _fields) return;
                _fields = value;
                OnPropertyChanged(nameof(Fields));
            }
        }

        ///<inheritdoc/>
        public override string ToString() => Name;

        /// <inheritdoc />
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises <see cref="PropertyChanged"/>
        /// </summary>
        /// <param name="propertyName"></param>
        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
